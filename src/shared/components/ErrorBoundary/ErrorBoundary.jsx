import React from "react";
import ErrorBanner from "../ErrorBanner";

class ErrorBoundary extends React.Component {
    state = {
        hasError: false,
    }

    static defaultProps = {
        banner: <ErrorBanner />,
    }

    componentDidCatch(error, errorInfo) {
        this.setState({hasError: true});
    }

    render() {
        const {children, banner} = this.props;
        const {hasError} = this.state;

        return !hasError ? children : banner;
    }
}

export default ErrorBoundary;