import React from "react";
import "./LoadingIndicator.scss";

const LoadingIndicator = () => {
    return (
        <div className="LoadingIndicator">
            <div className="LoadingIcon">
                <span className="LoadingBall" />
                <span className="LoadingBall" />
                <span className="LoadingBall" />
            </div>
            <span className="LoadingText">Loading</span>
        </div>
    )
}

export default LoadingIndicator;