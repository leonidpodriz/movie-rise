import React from "react";
import "./Logo.scss";

const Logo = () => {
    return (
        <a href="/" className="Logo">
            <span className="bold">Movie</span>
            <span className="thin">Rise</span>
        </a>
    )
}

export default Logo;