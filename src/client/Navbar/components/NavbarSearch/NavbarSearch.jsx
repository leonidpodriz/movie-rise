import React from "react";
import "./NavbarSearch.css";
import Button from "../../../../shared/components/Button";
import {ReactComponent as SearchIcon} from "../../../../assets/icons/search_icon.svg";

const NavbarSearch = () => {
    return (
        <div className="NavbarSearch">
            <Button icon={<SearchIcon />} type="icon" />
        </div>
    )
}

export default NavbarSearch;