import React from 'react';
import ReactDOM from 'react-dom';
import App from "./app/components/App";
import "normalize.css";
import "./assets/fonts/fonts.css"

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);